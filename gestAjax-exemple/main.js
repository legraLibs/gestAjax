/*!
*/
function main()
	{
//document.write('<h2>gestAjax.tableau()</h2>');
//document.write(gestAjax.tableau());

gestLib.write({lib:'gestAjax',txt:'--------------'});
charger_datas_txt();	// charger une 1ere fois
//document.write('<h2>gestAjax.tableau()</h2>');
//document.write(gestAjax.tableau());
gestLib.write({lib:'gestAjax',txt:'-1'});
charger_datas_js();	// charger une 1ere fois
gestLib.write({lib:'gestAjax',txt:'-2'});
charger_datas_js();	// charger une 2eme fois

gestLib.write({lib:'gestAjax',txt:'--------------'});
charger_datas_inexistante();



gestLib.write({lib:'gestAjax',txt:gestAjax.tableau()});
	}
	
/////////////////////////////////////////////////
function charger_datas_txt(forcer)
{
gestLib.write({lib:'gestAjax',txt:' charger_datas_txt(forcer)'});

gestAjax.loadFichier(
	{
	forcer:forcer
	,nom:'chargeTxt'
	,url:'./datas/datas-text.txt'
	,description:''
	,dataType:'text'
	,done:function(data){gestLib.inspect({lib:'gestAjax',varNom:'done:data',varPtr:data});}
	,always:function(data){gestLib.inspect({lib:'gestAjax',varNom:'done:always',varPtr:data});}
	});
gestLib.inspect({lib:'gestAjax',varNom:'gestAjax.erreur',varPtr:gestAjax.erreur});

}	// function charger_datas_class()

/////////////////////////////////////////////////
function charger_datas_js(forcer)
{
gestLib.write({lib:'gestAjax',txt:' charger_datas_js(forcer)'});

gestAjax.loadFichier(
	{
	forcer:forcer
	,nom:'chargeJS'
	,url:'./datas/datas-js.js'
	,description:''
	,dataType:'script'
	,done:function(data){gestLib.inspect({lib:'gestAjax',varNom:'done:data',varPtr:data});}
	,always:function(data){gestLib.inspect({lib:'gestAjax',varNom:'done:always',varPtr:data});}
	});
gestLib.inspect({lib:'gestAjax',varNom:'gestAjax.erreur',varPtr:gestAjax.erreur});

}	// function charger_datas_class()

/////////////////////////////////////////////////
function charger_datas_inexistante(forcer)
{
gestLib.write({lib:'gestAjax',txt:' charger_datas_inexistante(forcer)'});

gestAjax.loadFichier(
	{
	forcer:forcer
	,nom:'inexistante'
	,url:'./datas/datas-inexistante.js'
	,description:''
	,dataType:'text'
	,done:function(data){gestLib.inspect({lib:'gestAjax',varNom:'done:data',varPtr:data});}
	,always:function(data){gestLib.inspect({lib:'gestAjax',varNom:'done:always',varPtr:data});}
	});
gestLib.inspect({lib:'gestAjax',varNom:'gestAjax.erreur',varPtr:gestAjax.erreur});

}	// function charger_datas_class()


