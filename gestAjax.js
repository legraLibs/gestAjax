/*!
fichier: gestionAjax-0.2.js
version:0.2
auteur:pascal TOLEDO
date_creat: 2012.02.21
date_modif: 2013.08.11
source: http://legral.fr/intersites/lib/perso/js/
depend de:
	* jquery
description:
* gestion des lib charger
* fenetre de debug

*/
if(typeof(gestLib)==='object')
	gestLib.loadLib({nom:'gestAjax',ver:0.2,description:'gestion de la recursivité des chargements de fichiers via ajax'})
else gestLib=null;


//options: nom:obligatoire
//erreurNo:
// -1: absence d'url
// -10: la requete Ajax a echouer
function ajaxFichier(options)
	{
	gestLib.write({lib:'gestAjax',txt:' ajaxFichier()'});
	var dt=new Date();
	this.deb=dt.getTime();
	this.fin=null;	this.dur=null;
	this.fichierNb=0;
	this.erreurNo=0;
	this.isLoad=0;
	this.callNb=0;//nb de fois appeller
	this.loadNb=0;//nb de fois appeller et charger
	this.options=options;
	if(this.options.url==undefined){this.erreurNo=-1;}
	if(this.options.nom==undefined){this.erreurNo=-2;}
	this.nom=options.nom;
	this.url=options.url;
	this.dataType=options.dataType?options.dataType:'text';//xml|html|script|json|jsonp|text
	this.description=options.description;
	this.ver=(options.ver!=undefined)?options.ver:0;
	this.genre=(options.genre!=undefined)?options.genre:null;//texte,json,js
	this.status=0;//reponse ajax
	this.jqxhr=null;

	this.jqxhr=jQuery.ajax(this.options);
	this.jqxhr.done(function(data){
			if(typeof(options.done)==='function')options.done(data);
			})
	this.jqxhr.fail(function(){})
	this.jqxhr.always(function(data){
			if(typeof(options.always)==='function')options.always(data);
			})
		;
//	jqhxr.always(function(){});//2eme appels

	this.isLoad=1;
	dt=new Date();this.fin=dt.getTime();this.dur=this.fin-this.deb;

	return this;
	} // class AjaxFichier	

ajaxFichier.prototype={destruct:function(){this.ajax=undefined;}}

/*************************************************
*************************************************/
function TgestionAjax()
	{
	this.erreurNo=0;this.erreurTxt=new Array();
	this.fichierNb=0;
	this.fichiers=new Array();
	this.erreurTranslate();
	this.islibjqueryLoad=1;
	if(typeof(jQuery)!=='function'){this.erreurNo=1;this.islibjqueryLoad=0;}
	return this;
	}//class gestionAjax

TgestionAjax.prototype =
	{
	////////////////////////////////
	//Gestion des erreurs
	erreurTranslate:function()
	{
	this.erreurTxt['fr']=new Array();
	this.erreurTxt['fr'][0]='ok';
	this.erreurTxt['fr'][1]='lib jquery non chargé';
	this.erreurTxt['fr'][-1]='nom absent';
	}
	,erreurShow:function(errNu,lang)
		{
		return this.erreurNo+':'+this.erreurTxt[lang?lang:'fr'][errNu?errNu:this.erreurNo];
		}

	////////////////////////////////
	,destruct:function(fichier)
		{
		if(fichier)
			{
			if(this.fichiers[fichier])
				{
				this.fichiers[fichier].destruct();
				this.fichiers[fichier]='unload';
				}
			}

		else	//aucun fichier precise on les detruits tous!
			{
			var fichierNu=0;
			for(fichierr in this.fichiers)
				{
				this.fichiers[fichierr].destruct();
				this.fichiers[fichierr]=undefined;
				fichierNu++;
				if (fichierNu>=this.fichierNb){break;}
				}
			this.fichiers=undefined;
			this.fichiers=new Array();
			this.fichierNb=0;
			}
		},
	loadFichier:function(options)
		{
		gestLib.write({lib:'gestAjax',txt:'loadFichier()'});

		this.erreur=0;
		if(this.islibjqueryLoad===0){this.erreur=1;return this.erreur;}

		if(!options){this.erreur=-1;return this.erreur;}
		if(!options.nom){this.erreur=-2;return this.erreur;}
		if(this.fichiers[options.nom]==undefined || options.forcer)
			{
			this.destruct(options.nom);
			this.fichiers[options.nom]=new ajaxFichier(options);
			this.fichierNb++;

			if (this.fichiers[options.nom].jqxhr.status>=400)//erreur?
				{
				if (this.fichiers[options.nom].isLoad==0)
					{
					this.erreur=this.fichiers[options.nom].erreur;
					this.destruct(options.nom);
					}
				}
			}
		return this.erreur;
		},
	clean:function(nom)
		{var temp=new Array();//for(var key in this.fichiers){if (key!=fichier){temp[key]=this.fichiers[key];}}this.fichiers=temp;this.fichierNb--;
		for (var key in this.fichiers)
			{
			if(this.fichiers[key]!='unload'&&key!=nom){temp[key]=this.fichiers[key];}
			this.fichierNb--;
			}
		this.fichiers=temp;
		},
	tableau:function()
		{
		var out='';
		out+='<table><caption>fichiers JavaScript chargés dynamiquement('+this.fichierNb+')</caption>';
		out+='<thead><tr><th>genre</th><th>nom</th><th>version</th><th>status</th><th>durée</th>';
		out+='<th>description</th><th>url</th></tr><tbody>';
		var fd=0;//nb de fichier dechargé
		for (value in this.fichiers)
			{
			if (this.fichiers[value]==='unload')
				{
				fd++;
				out+='<tr><td></td><td>'+value+'</td><td></td><td>décharger</td><td></td>';
				out+='<td></td><td></td>';
				out+='</tr>\n';
				}
			else if (this.fichiers[value].nom)
				{
				url='<a href="'+this.fichiers[value].url+'">lien</a>';
				out+='<tr><td>'+this.fichiers[value].genre+'</td><td>'+this.fichiers[value].nom+'</td><td>'+this.fichiers[value].ver+'</td><td>'+this.fichiers[value].jqxhr.status+'</td><td>'+this.fichiers[value].dur+'</td>';
				out+='<td'+this.fichiers[value].description+'</td><td>'+url+'</td>';
				out+='</tr>\n';
				}
			};
		var a='';
		if(fd){a='<a href="javascript:gestAjax.clean();">nettoyer</a>';}
			{out+='</tbody><tfoot><tr><td colspan="7">nb de fichier dechargé:'+fd+' '+a+'</td></tr></tfoot></table>';}
		return out;
		}
	}

/* ************************************************************
	
   ***********************************************************/
gestAjax=new TgestionAjax();

if(typeof(gestLib)==='object')
	{
	gestLib.instanceAdd('gestAjax',gestAjax);//ajoute le pointeur de l'instance dans la liste des instances de la lib
	gestLib.end('gestAjax');
	}
